<?php

use Illuminate\Database\Seeder;

class loja extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('loja')->insert([
            
            'id'=> '15',
            'nome' => 'Perini Master',
            'endereco' => 'Av.Vasco da gama',
                   
        ]);
        DB::table('loja')->insert([
            
            'id'=> '5',
            'nome' => 'Perini Graça',
            'endereco' => 'Graça',
                   
        ]);       
    }
}
